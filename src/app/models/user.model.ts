export interface User {
    idUser: number;
    nameUser: string;
    typeUser: string;
    name: string;
    date: string;
    status: string;
  }
  