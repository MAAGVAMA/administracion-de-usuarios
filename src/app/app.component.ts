import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from './models/user.model';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'Administracion de Usuarios';

  users: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.getCars();
  }

  getCars() {
    this.userService.getUsers().subscribe((users) => {
     this.users = users['data'];
    });
  }
}
